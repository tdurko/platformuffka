using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeartUI : MonoBehaviour
{
    #region Singleton
    public static HeartUI instance;

    

    public void Awake()
    {
        if (instance != null)
        {
            Debug.LogWarning("More than One instance of HeartUI");
            return;
        }
        instance = this;
    }
    #endregion
    HeartSlot[] slots;

    void Start()
    {
        slots = this.GetComponentsInChildren<HeartSlot>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnLifeChange(int life)
    {
        Debug.Log(life);
        int k = life;
        for(int i = 0; i< slots.Length;i++)
        {
            if (i < k)
                slots[i].SetActive(true);
            else
                slots[i].SetActive(false);
        }
    }
}
