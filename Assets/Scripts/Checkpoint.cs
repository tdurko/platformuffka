using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checkpoint : MonoBehaviour
{
    public Transform transformPosition;
    void Start()
    {
        
    }

    public void Teleport(Transform player)
    {
        player.position = transformPosition.position;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Player.instance.SetCurrentCheckpoint(this);
        Debug.Log("Set Checkpoint =" + transform);
    }
}
