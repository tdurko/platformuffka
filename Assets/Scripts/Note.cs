﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Note : MonoBehaviour
{
    public float speed = 2f;
    public float magnitude = 20f;
    public float frequency = 0.5f;
    public int damage = 50;

    private  Rigidbody2D rigid;
    Vector3 pos;
    void Start()
    {
        rigid = GetComponent<Rigidbody2D>();
        pos = transform.position;

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log(collision.name);
        collision.gameObject.GetComponent<Enemy>().TakeDamage(damage);
        Destroy(gameObject);
    }



    // Update is called once per frame
    void FixedUpdate()
    {
        pos += transform.right * Time.deltaTime * speed;
        transform.position = pos + transform.up * Mathf.Sin(Time.time * frequency) * magnitude;
    }
}
