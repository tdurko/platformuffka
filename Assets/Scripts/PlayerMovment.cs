﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovment : MonoBehaviour
{
    PlayerController playerController;
    Animator animator;
    public float runSpeed = 200f;
    float horizontalMovment = 0f;
    bool jump = false;
    bool crouch = false;
    // Start is called before the first frame update
    void Start()
    {
        playerController = this.GetComponent<PlayerController>();
        animator = this.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        horizontalMovment = Input.GetAxisRaw("Horizontal") * runSpeed;
        animator.SetFloat("Speed", Mathf.Abs(horizontalMovment));


        if (Input.GetButtonDown("Jump"))
        {
            jump = true;
            animator.SetBool("IsJumping", true);
        }

        if(Input.GetKeyDown(KeyCode.C))
        {
            crouch = true;
            
        } else if (Input.GetKeyUp(KeyCode.C))
        {
            crouch = false;
        }

    }

    public void OnLanding()
    {
        animator.SetBool("IsJumping", false);
    }
    public void OnCrounching(bool isCrounching)
    {
        animator.SetBool("IsCrounching", isCrounching);
    }
    private void FixedUpdate()
    {
        playerController.Move(horizontalMovment * Time.fixedDeltaTime, crouch, jump);
        jump = false;
    }
}
