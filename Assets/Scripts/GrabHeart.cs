using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrabHeart : Collectable
{
    float time = 0f;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (Time.time >= time)
        {
            Player.instance.AddHeart();
            gameObject.SetActive(false);
            time = Time.time + 0.5f;
        }

    }
}
