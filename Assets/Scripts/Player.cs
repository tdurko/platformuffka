﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    #region Singleton
    public static Player instance;

    public void Awake()
    {
        if (instance != null)
        {
            Debug.LogWarning("More than One instance of Player");
            return;
        }
        instance = this;
    }
    #endregion
    Rigidbody2D rigid;
    public ResetCollectable resetCollectable;
    Animator animator;
    public Checkpoint startCheckpoint;
    Checkpoint currentCheckpoint;
    

    private int points = 0;
    private int life = 1;
    void Start()
    {
        rigid = gameObject.GetComponent<Rigidbody2D>();
        animator = gameObject.GetComponent<Animator>();
        currentCheckpoint = startCheckpoint;
        startCheckpoint.Teleport(transform);
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void TakeDamage(int damage)
    {
        if(damage == 99)
        {
            life = 0;
        }

        animator.SetTrigger("Hurt");
        Debug.Log("Player taking damage " + damage);
        life--;
        if(life <= 0)
        {
            HeartUI.instance.OnLifeChange(1);
            startCheckpoint.Teleport(transform);
            currentCheckpoint = startCheckpoint;
            resetCollectable.ResetObjects();
            resetCollectable.ResetPoints();
            life = 1;
        }
        else
        {
            HeartUI.instance.OnLifeChange(life);
            currentCheckpoint.Teleport(transform);
        }
        

    }

    public void SetCurrentCheckpoint(Checkpoint checkpoint)
    {
        currentCheckpoint = checkpoint;
    }

    public void AddHeart()
    {
        Debug.Log("Add one serce");
        life++;
        HeartUI.instance.OnLifeChange(life);
    }

    public void AddPoint()
    {
        points +=10;
        PointsUI.instance.SetPoints(points);
    }

    public void SetPoints(int value)
    {
        points = value;
        PointsUI.instance.SetPoints(points);
    }

    public int GetPoints()
    {
        return points;
    }

    public void SetData(int points, int hearts)
    {
        this.points = points;
        life = hearts;
    }

    public int GetHearts()
    {
        return life;
    }

    public void AddForceOnPlayer(Vector2 vector, int force)
    {
        Vector2 vector2 = new Vector2(vector.x * force * Time.deltaTime* 2, vector.y * force * Time.deltaTime);
        Debug.Log(vector2);
        rigid.AddForce(vector2);
    }
}
