using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerData
{
    public PlayerData(Player player)
    {
        this.points = player.GetPoints();
        this.hearts = player.GetHearts();
    }

    int points;
    int hearts;

    public void GetPlayerData(Player player)
    {
        player.SetData(points, hearts);
    }

    public void GetPlayerData(out int points, out int hearts)
    {
        points = this.points;
        hearts = this.hearts;
    }
}
