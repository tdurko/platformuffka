using UnityEngine.SceneManagement;
using UnityEngine;
public class StartMenu : MonoBehaviour
{
    public void Play()
    {
        SceneManager.LoadScene("GameScene");
    }

    public void Exit()
    {
        Application.Quit();
    }

}
