using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpossumTP : MonoBehaviour
{
    Vector3 startPosition;
    public float distanceStop = 4;

    Rigidbody2D rigid;
    void Start()
    {
        startPosition = transform.position;
        rigid = gameObject.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.position.x > startPosition.x + distanceStop && transform.position.y < startPosition.y - 2) 
        {
            rigid.MovePosition(new Vector2(transform.position.x, startPosition.y));
        }
    }
}
