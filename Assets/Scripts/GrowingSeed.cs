using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrowingSeed : MonoBehaviour
{
    bool playerInRange = false;

    public GameObject lader;
    public GameObject canvasObject;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(playerInRange)
        {
            Debug.Log("Press GGGG");
            if(Input.GetKeyDown(KeyCode.G))
            {
                Grow();
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        playerInRange = true;
        canvasObject.SetActive(true);
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        playerInRange = false;
        canvasObject.SetActive(false);
    }

    public void Grow()
    {
        lader.SetActive(true);
        GameObject.Destroy(canvasObject);
        GameObject.Destroy(gameObject);
    }
}
