﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FluteShot : AttackController
{
    public GameObject bulletPrefab;
    public override void Attack()
    {
        Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
    }
}
