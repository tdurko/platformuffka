﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInRange : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("Opsos podaza za player");
        gameObject.GetComponentInParent<EnemyMovment>().SetFollow(true);
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        gameObject.GetComponentInParent<EnemyMovment>().SetFollow(false);
    }
}
