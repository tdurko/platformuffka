﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollideOnTouch : MonoBehaviour
{
    public int damage = 40;
    Collider2D collider2;
    public int force = 100;
    float time = 0f;

    Vector2 P;
    Vector2 F;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(Time.time >= time)
        {
            F = collision.gameObject.GetComponent<Collider2D>().ClosestPoint(transform.position);
            P = new Vector2(collision.gameObject.transform.position.x, collision.gameObject.transform.position.y);
            Debug.Log(P - F);
            P = P - F;
            P.Normalize();
            collision.gameObject.GetComponent<Player>().AddForceOnPlayer(P, force);
            collision.gameObject.GetComponent<Player>().TakeDamage(damage);

            time = Time.time + 0.5f;
        }
        

    }

    
}
