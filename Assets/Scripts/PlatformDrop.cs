using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformDrop : MonoBehaviour
{
    bool fall = false;
    bool returned = false;
    public GameObject parentGameObject;
    Rigidbody2D rigid;
    BoxCollider2D box;
    public float timeValue = 4;
    float fallTime;
    Vector3 startPoint;

    void Start()
    {
        startPoint = new Vector3(parentGameObject.transform.position.x, parentGameObject.transform.position.y, parentGameObject.transform.position.z);
        fallTime = timeValue;
    }

    private void Update()
    {

        if (Time.time >= fallTime && fall)
        {
            Drop();
            fallTime += 3 * timeValue;
        }
        if (Time.time >= fallTime && returned)
        {
            Return();
        }



    }

    public void Drop()
    {
        parentGameObject.AddComponent<Rigidbody2D>();
        rigid = parentGameObject.GetComponent<Rigidbody2D>();
        box = parentGameObject.GetComponent<BoxCollider2D>();
        this.gameObject.GetComponent<BoxCollider2D>().enabled = false;
        box.enabled = false;
        rigid.gravityScale = 5;
        returned = true;
        fall = false;

    }

    public void Return()
    {
        Destroy(parentGameObject.GetComponent<Rigidbody2D>());
        this.gameObject.GetComponent<BoxCollider2D>().enabled = true;
        box.enabled = true;
        parentGameObject.transform.position = startPoint;
        returned = false;
        fallTime = timeValue;


    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        

        if(!fall)
        {
            fallTime += Time.time;
            fall = true;
            }
    }
}
