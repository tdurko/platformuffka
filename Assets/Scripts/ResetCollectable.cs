using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetCollectable : MonoBehaviour
{
    Collectable[] TableofGameObjects;
    private void Start()
    {
        TableofGameObjects = gameObject.GetComponentsInChildren<Collectable>();
    }
    private void Update()
    {
        
    }

    public void ResetObjects()
    {
        
        foreach (var point in TableofGameObjects)
        {
            
            point.gameObject.SetActive(true);
        }
    }

    public void ResetPoints()
    {
        Player.instance.SetPoints(0);
    }
}
