using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PointsUI : MonoBehaviour
{
    #region Singleton
    public static PointsUI instance;
    TextMeshProUGUI text;


    public void Awake()
    {
        if (instance != null)
        {
            Debug.LogWarning("More than One instance of HeartUI");
            return;
        }
        instance = this;
    }
    #endregion
    void Start()
    {
        text = GetComponentInChildren<TextMeshProUGUI>();
        text.text = Player.instance.GetPoints().ToString();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetPoints(int points)
    {
        text.text = points.ToString();
    }
}
