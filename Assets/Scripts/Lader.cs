﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lader : MonoBehaviour
{
    bool isOnLader = false;
    float verticalMovment = 0;
    public PlayerController playerController;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        verticalMovment = Input.GetAxisRaw("Vertical");
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        isOnLader = true;
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        isOnLader = false;
        playerController.StopClimb(isOnLader);

    }

    private void FixedUpdate()
    {
        if(isOnLader)
        {
            
            playerController.Climb(verticalMovment, isOnLader);
            
        }
    }
}
