﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovment : MonoBehaviour
{
    public float speed = 2.5f;
    public float patrolSpeed = 100f;
    public float distance = 100f;
    public Transform player;
    Rigidbody2D rb;
    Vector2 newPos;
    float targetPosLeft;
    float targetPosRight;

    bool isFlipped = true;
    public bool followPlayer = false;
    void Start()
    {
        rb = gameObject.GetComponent<Rigidbody2D>();
        newPos = rb.position;
        targetPosLeft = rb.position.x - distance;
        targetPosRight = rb.position.x + distance;
    }

    // Update is called once per frame
    void Update()
    {
        if (followPlayer)
        {
            rb.velocity = new Vector2(0, 0);
            Vector2 target = new Vector2(player.position.x, rb.position.y);
            newPos = Vector2.MoveTowards(rb.position, target, speed * Time.fixedDeltaTime);
        }
        else
        {
            
            if(targetPosRight <= transform.position.x && !isFlipped)
            {
                Debug.Log("Flip oposa");
                Vector3 flipped = transform.localScale;
                flipped.z *= -1f;
                transform.localScale = flipped;
                transform.Rotate(0f, 180f, 0f);
                isFlipped = true;
            }

            if (targetPosLeft >= transform.position.x && isFlipped)
            {
                Debug.Log("Flip oposa");
                Vector3 flipped = transform.localScale;
                flipped.z *= -1f;
                transform.localScale = flipped;
                transform.Rotate(0f, 180f, 0f);
                isFlipped = false;
            }
        }
    }
    private void FixedUpdate()
    {
        if (followPlayer)
        {
            rb.MovePosition(newPos);
            LookAtPlayer();
        }
        else
        {
            Debug.Log("Biegnij oposie");
            if(isFlipped)
            {
                Debug.Log("LEWO");
                rb.velocity = new Vector2(-patrolSpeed * Time.fixedDeltaTime, rb.velocity.y);
            }
            else
            {
                Debug.Log("PRAWO");
                rb.velocity = new Vector2(patrolSpeed * Time.fixedDeltaTime, rb.velocity.y);
            }
            
        }
            
    }

    public void LookAtPlayer()
    {
        Vector3 flipped = transform.localScale;
        flipped.z *= -1f;

        if(transform.position.x > player.position.x && !isFlipped)
        {
            transform.localScale = flipped;
            transform.Rotate(0f, 180f, 0f);
            isFlipped = true;
        }
        else if(transform.position.x < player.position.x && isFlipped)
        {
            transform.localScale = flipped;
            transform.Rotate(0f, 180f, 0f);
            isFlipped = false;
        }
    }

    public void SetFollow(bool follow)
    {
        followPlayer = follow;
    }
}
