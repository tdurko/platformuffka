using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MapEnd : MonoBehaviour
{

    void End()
    {
        SaveSystem.SavePlayer();
        SceneManager.LoadScene("EndScene");
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        End();
    }
}
