using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class EndMenu : MonoBehaviour
{
    public TextMeshProUGUI pointsText;
    public TextMeshProUGUI heartsText;

    int points;
    int hearts;
    PlayerData playerData;
    void Start()
    {

        playerData = SaveSystem.LoadPlayer();
        playerData.GetPlayerData(out points, out hearts);
        pointsText.text = "= " + points;
        heartsText.text = "= " + hearts;
    }

    public void ReturnButton()
    {
        SceneManager.LoadScene("MenuScene");
    }
}
